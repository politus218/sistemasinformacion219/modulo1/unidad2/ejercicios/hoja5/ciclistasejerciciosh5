﻿-- CONSULTAS  DE COMBINACIONES EXTERNAS

  -- Nombre y edad de los ciclistas que NO han ganado etapas.

  SELECT DISTINCT c.nombre, c.edad FROM ciclista c
  LEFT JOIN etapa e
  ON c.dorsal = e.dorsal
  WHERE e.dorsal IS NULL;  

  -- Nombre y edad de los ciclistas que no han ganado puertos.

  SELECT DISTINCT c.nombre, c.edad FROM ciclista c
  LEFT JOIN puerto p
  ON c.dorsal = p.dorsal
  WHERE p.dorsal IS NULL;

  -- Listar el director de los equipos que tengan ciclistas que no hayan ganado ninguna etapa.

  SELECT DISTINCT e.director FROM equipo e
  JOIN (ciclista c
  LEFT JOIN etapa e1
  ON e1.dorsal = c.dorsal)
  ON c.nomequipo = e.nomequipo
  WHERE ((e1.dorsal) IS NULL);

  -- Listar el dorsal y el nombre de los ciclistas que no hayan llevado algún maillot.

  SELECT DISTINCT c.dorsal, c.nombre FROM ciclista c
  LEFT JOIN lleva l
  ON l.dorsal = c.dorsal
  WHERE ((l.dorsal) IS NULL);

  -- Dorsal de los ciclistas que no hayan llevado el maillot amarillo.

 SELECT DISTINCT l.dorsal FROM maillot m
 JOIN lleva l
 ON m.código = l.código
 WHERE m.color != 'amarillo';

-- Indicar el numetapa de las etapas que no tengan puertos.

  SELECT DISTINCT e.numetapa FROM etapa e
  LEFT JOIN puerto p
  ON e.numetapa = p.numetapa
  WHERE p.nompuerto IS NULL;

-- Indicar la distancia media de las etapas que no tengan puertos.

  SELECT DISTINCT AVG(e.kms)  FROM etapa e
  LEFT JOIN puerto p
  ON e.numetapa = p.numetapa
  WHERE p.nompuerto IS NULL;

-- Listar el nº de ciclistas que no hayan ganado alguna etapa.

  SELECT COUNT(c.dorsal) FROM ciclista c
  LEFT JOIN etapa e
  ON c.dorsal = e.numetapa
  WHERE e.numetapa IS NULL;

-- Listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tenga puerto.

  SELECT DISTINCT c.dorsal FROM (ciclista c
  JOIN etapa e
  ON c.dorsal = e.dorsal)
  LEFT JOIN puerto p
  ON e.numetapa = p.numetapa
  WHERE p.numetapa IS NULL;

-- Listar el dorsal de los ciclistas que hayan ganado únicamente etapas que no tengan puertos.

  SELECT DISTINCT c.dorsal FROM ciclista c
  JOIN (etapa e LEFT JOIN puerto p ON e.numetapa = p.numetapa)
  ON c.dorsal = e.dorsal
  WHERE p.numetapa IS NULL;
  





 

  

  

  